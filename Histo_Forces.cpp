/****************************************************************************

		    RELATIONS SPATIALES DIRECTIONNELLES
		     (P.Matsakis/L.Wendling/J.Desachy)


     Execution de notre methode.
     ===========================


1) Hypothese H1 simple :
   ---------------------
  Hypothese multiplicative. On reobient un histogramme proche de Miyajima
(la difference venant de la prise en compte du facteur isotropique).

2) Hypothese H2 simple : 
   ---------------------
  On ne tient compte que des objets non overlappe et uniquement du log.

3) Hypothese H2 :
   --------------
  Tous les cas de figure sont pris en compte. Gestion des overlaps et linearite
de la fonction du ln (H2 simple) au produit (hypothese H1).

4) Hypothese H2_Floue_1 :
   ----------------------
  Double sommation. segment de 1 unitee. - minimum des valeurs en facteur -

5) Hypothese H2 Floue_2 :
   ----------------------
  Simple sommation (style Khrisnapuram). segment de longueur variable. Appel
a l'hypothese H2.

6) Hypothese H2 Floue_3 :
   ----------------------
  Double sommation (Hyp dub * prad). segment de longueur variable. Appel
a l'hypothese H2. 

7) Hypothese H1
   --------------
  Tous les cas de figure sont pris en compte. Gestion des overlaps et linearite

8) Hypothese H1 floue_1 :
   ----------------------
 Simple sommation (style Khrisnapuram). segment de longueur variable. Appel
a l'hypothese H1.

9) Hypothese H2_Poly :
   -------------------
   Prise en compte des polygones non overlap.


=============================================================================

 Date   | avril 1996
 Auteur | L. Wendling

============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include "Structure.h" 

/*******************/  
int val_abs(int x)   
{
  if (x>0) return(x);
  else return (-x);
}


/*****/
double minimum(int A, int B)
{
  if (A<B)
    return((double)A/255.);
  else
    return((double)B/255.);
}


/* -1, 0, 1 */
int Sign(int x)
{
  if (x==0)
    return(0);
  else
    if (x<0) return(-1);
    else return(1);
}


/* Pour accelerer le traitement par la suite... */
/**************************************************************/
/****** Pointeur sur ligne de l'image *************************/
/**************************************************************/

void Cree_Tab_Pointeur(DATA *Image, struct Adresse *Tab, int Xsize, int Ysize)
{
  int i,j;
  i=Xsize*Ysize;
  for (j=0; j<Ysize; j++)
    {
      i-=Xsize;
      Tab[j].adr=Image+i;
    }
}


/**************************************************************/
/* Creation d'un tableau ln de constante - cas floue          */
/**************************************************************/

void Cree_Tab_ln(double *Tab, int Xsize)
{
  int i;
  for(i=1;i<Xsize;i++)
    *(Tab+i)=log((double)(i+1)*(i+1)/(i*(i+2)));
}


/* Pas de struct point pour aller plus vite */
/**************************************************************/
/******** Bresenham arret suivant l'axe des Y et sauvegarde ***/
/******** dans structure chaine des changements             ***/
/**************************************************************/

void Bresenham_X(int x1, int y1, int x2, int y2, int Borne_X, int *chaine)
	       
{
  static int Tmp,e,change,x,y,Delta_x,Delta_y,s1,s2,x_prec,y_prec;
  x=x1;
  y=y1;
  Delta_x = val_abs(x2-x1);
  Delta_y = val_abs(y2-y1);
  s1 = Sign(x2-x1);
  s2 = Sign(y2-y1);
  x_prec=x;
  y_prec=y;
  chaine[0]=0;

  /* Intervertion de delta_x et delta_y suivant la pente de seg. */
  if (Delta_y>Delta_x)
    {
      Tmp     = Delta_x;
      Delta_x = Delta_y;
      Delta_y = Tmp;
      change = 1;
    }
  else
    change = 0;
  /* init. de e (cas inters. avec -1/2) */
  e=2*Delta_y-Delta_x;
  
  while (x < Borne_X)
    {
      while (e>=0)
	{
	  if (change)
	    {
	      x=x+s1;
	      chaine[0]++;
	      chaine[chaine[0]]=val_abs(y-y_prec)+1;
	      chaine[0]++;
	      chaine[chaine[0]]=1;
	      y_prec=y+1; 
	    }
	  else
	    {
	      y=y+s2;
	      chaine[0]++;
	      chaine[chaine[0]]=val_abs(x-x_prec)+1;
	      chaine[0]++;
	      chaine[chaine[0]]=1;
	      x_prec=x+1; 
	    }
	  e=e-2*Delta_x;
	}
      if (change) 
	y=y+s2;
      else
	x=x+s1;
      e=e+2*Delta_y;
    }
  if (change)
    if (y_prec!=y)
      {
	chaine[0]++;
	chaine[chaine[0]]=val_abs(y-y_prec)+1;
      }
    else {}
  else
    if (x_prec!=x)
      {
	chaine[0]++;
	chaine[chaine[0]]=val_abs(x-x_prec)+1;
      }
}


/**************************************************************/
/**** Meme fct mais arret suivant l'axe des Y               ***/
/**************************************************************/

void Bresenham_Y(int x1, int y1, int x2, int y2, int Borne_Y, int *chaine)
	       
{
  int Tmp,e,change,x,y,Delta_x,Delta_y,s1,s2,x_prec,y_prec;
  x=x1;
  y=y1;
  Delta_x = val_abs(x2-x1);
  Delta_y = val_abs(y2-y1);
  s1 = Sign(x2-x1);
  s2 = Sign(y2-y1);
  x_prec=x;
  y_prec=y;
  chaine[0]=0;

  /* Intervertion de delta_x et delta_y suivant la pente de seg. */
  if (Delta_y>Delta_x)
    {
      Tmp     = Delta_x;
      Delta_x = Delta_y;
      Delta_y = Tmp;
      change = 1;
    }
  else
    change = 0;
  /* init. de e (cas inters. avec -1/2) */
  e=2*Delta_y-Delta_x;
  
  while (y < Borne_Y)
    {
      while (e>=0)
	{
	  if (change)
	    {
	      x=x+s1;
	      chaine[0]++;
	      chaine[chaine[0]]=val_abs(y-y_prec)+1;
	      chaine[0]++;
	      chaine[chaine[0]]=1;
	      y_prec=y+1;
	    }
	  else
	    {
	      y=y+s2;
	      chaine[0]++;
	      chaine[chaine[0]]=val_abs(x-x_prec)+1;
	      chaine[0]++;
	      chaine[chaine[0]]=1;
	      x_prec=x+1; 
	    }
	  e=e-2*Delta_x;
	}
      if (change) 
	y=y+s2;
      else
	x=x+s1;
      e=e+2*Delta_y;
    }
  if (change)
    if (y_prec!=y)
      {
	chaine[0]++;
	chaine[chaine[0]]=val_abs(y-y_prec)+1;
      }
    else {}
  else
    if (x_prec!=x)
      {
	chaine[0]++;
	chaine[chaine[0]]=val_abs(x-x_prec)+1;
      }
}


/**************************************************************/
/*********** On trace une ligne d'apres bresenham *************/
/*********** Decalage en X - et coupe de niveau   *************/
/**************************************************************/

struct Segment *ligne_x(struct Adresse *Tab,  int x, int y, int Borne_X, int Borne_Y, int *Chaine, int deb, struct Segment *Liste_Seg, int pas_x, int pas_y, int Cut)
{
  int i;
  int S;
  struct Segment *Deb_liste, *Seg;
  int paspasser; /* pas bo ! a optimiser (gestion diff. des TWL)... */
  int xtmp = 0,ytmp = 0, xprec = 0, yprec = 0;
  paspasser=1;
  S=0;
  Liste_Seg=Deb_liste=NULL;
  while (((x!=Borne_X)&&(y!=Borne_Y)) && (deb<=Chaine[0]))
    {
      i=0;
      while ((i<Chaine[deb])&&((x!=Borne_X)&&(y!=Borne_Y)))
	{
	  if (*(Tab[y].adr+x)>=Cut)
	    if (!S)
	      {
		S=1;
		xtmp=xprec=x;
		ytmp=yprec=y;
	      }
	    else {xprec=x; yprec=y;} 
	  else
	    if (S)
	      {
		S=0;
		Seg=(struct Segment *)malloc(sizeof(struct Segment));
		Seg->x1=xtmp;
		Seg->y1=ytmp;
		Seg->x2=xprec;
		Seg->y2=yprec;
		if (paspasser)
		  {
		    Liste_Seg=Deb_liste=Seg;
		    Liste_Seg->suivant=NULL;
		    paspasser=0;
		  }
		else
		  {
		    Liste_Seg->suivant=Seg;
		    Liste_Seg=Liste_Seg->suivant;
		    Liste_Seg->suivant=NULL;
		  }
	      }
	  i++;	
	  x+=pas_x;
	}
      deb++;
      y+=pas_y;
      deb++;
    }
  /* Segment limitrophe a la fenetre */
 if (S)
    {
      Seg=(struct Segment *)malloc(sizeof(struct Segment));
      Seg->x1=xtmp;
      Seg->y1=ytmp;
      Seg->x2=xprec;
      Seg->y2=yprec;
      if (paspasser)
	{
	  Liste_Seg=Deb_liste=Seg;
	  Liste_Seg->suivant=NULL;
	  paspasser=0;
	}
      else
	{
	  Liste_Seg->suivant=Seg;
	  Liste_Seg=Liste_Seg->suivant;
	  Liste_Seg->suivant=NULL;
        }
    }
  return(Deb_liste);
}


/**************************************************************/
/* On trace une ligne - cas floue -                           */
/* Decalage en X   - sauvegarde pt a pt                       */
/**************************************************************/

struct Segment *ligne_x_floue(struct Adresse *Tab,  int x, int y, int Borne_X, int Borne_Y, int *Chaine, int deb, struct Segment *Liste_Seg, int pas_x, int pas_y)
{
  int i;
  struct Segment *Deb_liste, *Seg;
  int paspasser; /* pas bo ! a optimiser... */
  paspasser=1;
  Liste_Seg=Deb_liste=NULL;

  while (((x!=Borne_X)&&(y!=Borne_Y)) && (deb<=Chaine[0]))
    {
      i=0;
      while ((i<Chaine[deb])&&((x!=Borne_X)&&(y!=Borne_Y)))
	{
	  if (*(Tab[y].adr+x)!=0)
	    {
	      Seg=(struct Segment *)malloc(sizeof(struct Segment));
	      Seg->x1=x;
	      Seg->y1=y;
	      Seg->x2=x;
	      Seg->y2=y;
	      Seg->Val=(int) *(Tab[y].adr+x);
		if (paspasser)
		  {
		    Liste_Seg=Deb_liste=Seg;
		    Liste_Seg->suivant=NULL;
		    paspasser=0;
		  }
		else
		  {
		    Liste_Seg->suivant=Seg;
		    Liste_Seg=Liste_Seg->suivant;
		    Liste_Seg->suivant=NULL;
		  }
	    }
	  i++;	
	  x+=pas_x;
	}
      deb++;
      y+=pas_y;
      deb++;
    }
  return(Deb_liste);
}


/**************************************************************/
/*********** Idem fct prec. mais decalage en Y  ***************/
/**************************************************************/

struct Segment *ligne_y(struct Adresse *Tab, int x, int y, int Borne_X, int Borne_Y, int *Chaine, int deb, struct Segment *Liste_Seg, int pas_x, int pas_y, int Cut)
{
  int i;
  int S=0;
  struct Segment *Deb_liste, *Seg;
  int paspasser; /* pas bo ! a optimiser... */
  int xtmp = 0, ytmp = 0, xprec = 0, yprec = 0;
  paspasser=1;
  Liste_Seg=Deb_liste=NULL;

  while (((x!=Borne_X)&&(y!=Borne_Y)) && (deb<=Chaine[0]))
    {
      i=0;
      while ((i<Chaine[deb])&&((x!=Borne_X)&&(y!=Borne_Y)))
	{
	  if (*(Tab[y].adr+x)>=Cut)
	    if (!S)
	      {
		S=1;
		xtmp=xprec=x;
		ytmp=yprec=y;
	      }
	    else {xprec=x;yprec=y;} 
	  else
	    if (S)
	      {
		S=0;
		Seg=(struct Segment *)malloc(sizeof(struct Segment));
		/* permut. de x et y - projection suivant y - */
		Seg->x1=ytmp; 
		Seg->y1=xtmp;
		Seg->x2=yprec; 
		Seg->y2=xprec;
		if (paspasser)
		  {
		    Liste_Seg=Deb_liste=Seg;
		    Liste_Seg->suivant=NULL;
		    paspasser=0;
		  }
		else
		  {
		    Liste_Seg->suivant=Seg;
		    Liste_Seg=Liste_Seg->suivant;
		    Liste_Seg->suivant=NULL;
		  }
	      }
	  i++;
	  y+=pas_y;
	}
      deb++;
      x+=pas_x;
      deb++;
    }
  /* Segment limitrophe a la fenetre */
  if (S)
    {
      Seg=(struct Segment *)malloc(sizeof(struct Segment));
      Seg->x1=ytmp; 
      Seg->y1=xtmp;
      Seg->x2=yprec;  
      Seg->y2=xprec;
      if (paspasser)
	{
	  Liste_Seg=Deb_liste=Seg;
	  Liste_Seg->suivant=NULL;
	  paspasser=0;
	}
      else
	{
	  Liste_Seg->suivant=Seg;
	  Liste_Seg=Liste_Seg->suivant;
	  Liste_Seg->suivant=NULL;
	}
    }
  return(Deb_liste);
}


/**************************************************************/
/* Idem fct prec. - cas flou - mais decalage en Y             */
/**************************************************************/

struct Segment *ligne_y_floue(struct Adresse *Tab, int x, int y, int Borne_X, int Borne_Y, int *Chaine, int deb, struct Segment *Liste_Seg, int pas_x, int pas_y)
{
  int i;
  int xtmp;
  int ytmp;
  struct Segment *Deb_liste, *Seg;
  int paspasser; /* pas bo ! a optimiser... */
  paspasser=1;
  xtmp=ytmp=0;
  Liste_Seg=Deb_liste=NULL;
   
  while (((x!=Borne_X)&&(y!=Borne_Y)) && (deb<=Chaine[0]))
    {
      i=0;
      while ((i<Chaine[deb])&&((x!=Borne_X)&&(y!=Borne_Y)))
	{
	  if (*(Tab[y].adr+x)!=0)
	    {
	      Seg=(struct Segment *)malloc(sizeof(struct Segment));
	      /* permut. de x et y - projection suivant y - */
	      Seg->x1=y;
	      Seg->y1=x;
	      Seg->x2=y;
	      Seg->y2=x;
	      Seg->Val=(int) *(Tab[y].adr+x);
	      if (paspasser)
		{
		  Liste_Seg=Deb_liste=Seg;
		  Liste_Seg->suivant=NULL;
		  paspasser=0;
		}
	      else
		{
		  Liste_Seg->suivant=Seg;
		  Liste_Seg=Liste_Seg->suivant;
		  Liste_Seg->suivant=NULL;
		}
	    }
	  i++;
	  y+=pas_y;
	}
      deb++;
      x+=pas_x;
      deb++;
    }
  return(Deb_liste);
}


/**************************************************************/
/* Creation de l'histogramme (Miyajima) - Hypothese H1 -      */
/**************************************************************/

void H1_Simple(double *Histo, struct Segment *List_Seg_A, struct Segment *List_Seg_B, int Case1, int Case2)
{
  static double Som_Seg_Pos;
  static double Som_Seg_Neg;
  static double d1,d2;
  
  struct Segment *Seg_A, *Seg_B, *pt_L_A, *pt_L_B;

  Som_Seg_Pos = Som_Seg_Neg = 0;
  
  pt_L_A=List_Seg_A;
  
  while (pt_L_A)
    {
      Seg_A=pt_L_A;
      /* premier segment */
      d1=val_abs(Seg_A->x2-Seg_A->x1)+1;

      pt_L_B=List_Seg_B;
      while (pt_L_B)
	{          
	  Seg_B=pt_L_B;
	  d2=val_abs(Seg_B->x2-Seg_B->x1)+1;
	  /* d'abord => Pas de chevauchement */
	  if (Seg_A->x2 < Seg_B->x1) 
	    Som_Seg_Pos += d1*d2; 
	  else	       
	      Som_Seg_Neg += d1*d2;  
	  pt_L_B=pt_L_B->suivant; 
	}
      pt_L_A=pt_L_A->suivant; 
    }
  /* Attribution de la sommation a l'histo. */
  Histo[Case1] += Som_Seg_Pos;
  /* Angle oppose. */
  Histo[Case2] += Som_Seg_Neg; 
}


/**************************************************************/
/* Creation de l'histogramme (fct ln) - Hypothese H2 simple - */
/**************************************************************/

void H2_Simple(double *Histo, struct Segment *List_Seg_A, struct Segment *List_Seg_B, int Case1, int Case2, double Fac)
{
  static double Som_Seg_Pos;
  static double Som_Seg_Neg;
  static double d1,d2,D;
  
  struct Segment *Seg_A, *Seg_B, *pt_L_A, *pt_L_B;

  Som_Seg_Pos = Som_Seg_Neg = 0;
  
  pt_L_A=List_Seg_A;

  while (pt_L_A)
    {
      Seg_A=pt_L_A;
      d1=val_abs(Seg_A->x2-Seg_A->x1)+1;
      /* premier segment */
      pt_L_B=List_Seg_B;
      while (pt_L_B)
	{          
	  Seg_B=pt_L_B;
	  d2=val_abs(Seg_B->x2-Seg_B->x1)+1;
	  /* d'abord => Pas de chevauchement */
	  if (Seg_A->x2 < Seg_B->x1)  
	    { 
	      D=val_abs(Seg_B->x1-Seg_A->x2)-1; 
	      Som_Seg_Pos += log((D+d1)*(D+d2)/(D*(D+d1+d2))); 
	    } 
	  else 	      
	    { 
	      D=val_abs(Seg_B->x2-Seg_A->x1)-1;  
	      Som_Seg_Neg += log((D+d1)*(D+d2)/(D*(D+d1+d2)));  
	    }	
	  pt_L_B=pt_L_B->suivant; 
	} 
      pt_L_A=pt_L_A->suivant; 
    }
  /* Attribution de la sommation a l'histo. */
  Histo[Case1] += Som_Seg_Pos*Fac;
  /* Angle oppose. */
  Histo[Case2] += Som_Seg_Neg*Fac; 
}


/**************************************************************/
/* Seg A Precede le seg B                                     */
/**************************************************************/

void Before(double *H, double x, double y, double z, double Y0, int Case, double Poids, double *Som_LN)
{
  if (Y0<=y)
    (*Som_LN) += log((x+y)*(y+z)/(y*(x+y+z))) * Poids; /* Y0^2 en fac... */
  /*H[Case].Val +=Y0*Y0*log((x+y)*(y+z)/(y*(x+y+z))) * Poids;  */
  else
    if (Y0>=x+y+z)
      H[Case] += x*z*Poids;
    else
      if ((Y0<=x+y)&&(Y0<=y+z))
	H[Case] += (Y0*Y0 * log((y+z)*(x+y)/(Y0*(x+y+z)))+
			(y-3*Y0)*(y-Y0)/2)*Poids;
      else
	if ((Y0>=x+y)&&(Y0<=y+z))
	  H[Case] += (Y0*Y0 * log((y+z)/(x+y+z))-(x/2+y-2*Y0)*x)*Poids;
	else
	  if (((Y0>=x+y)&&(Y0>=y+z))&&(Y0<=x+y+z))
	    H[Case] += (Y0*Y0 * log(Y0/(x+y+z))-
	      (x+y+z-3*Y0)*(x+y+z-Y0)/2+x*z)*Poids;
	  else
	    /*   if ((Y0>=x+y)&&(Y0>=y+z)) */
	    H[Case] += (Y0*Y0 * log((y+x)/(x+y+z))-(z/2+y-2*Y0)*z)*Poids;
}


/**************************************************************/
/* non inclusion totale des ensembles                         */
/**************************************************************/

void Overlaps(double *H, double x, double y, double z, double Y0, int Case, double Poids)
{
  if ((Y0<=x+y)&&(Y0<=y+z))
    H[Case] += (Y0*Y0 * log((x+y)*(y+z)/(Y0*(x+y+z)))-
		    2*Y0*(y-3*Y0/4))*Poids;
  else
    if (((Y0<=x+y)&&(y+z<=Y0))&&(0<=y+z))
      H[Case] += (Y0*Y0 * log((y+x)/(x+y+z))+2*Y0*z-(y+z)*(y+z)/2)*Poids;
    else
      if (((x+y<=Y0)&&(Y0<=y+z))&&(0<=x+y))
	H[Case] += (Y0*Y0 * log((y+z)/(x+y+z))+2*Y0*x-(y+x)*(y+x)/2)*Poids;
      else
	if (((x+y<=Y0)&&(y+z<=Y0))&&(Y0<=x+y+z))
	  H[Case] += (Y0*Y0 * log(Y0/(x+y+z))-(x+y+z-3*Y0)*(x+y+z-Y0)/2+
			  x*z-y*y/2)*Poids;
	else
	  H[Case] += (x*z-y*y/2)*Poids;
}


/**************************************************************/
/* inclusion totale de A et B                                 */
/**************************************************************/

void Overlapped_By(double *H, double x, double y, double z, double Y0, int Case, double Poids)
{
  if (Y0>=x+y+z)
    H[Case] += ((x+y+z)*(x+y+z)/2)*Poids;
  else
    H[Case] += (Y0*Y0 * log(Y0/(x+y+z))+2*Y0*(x+y+z-3*Y0/4))*Poids;
}


/**************************************************************/
/* Seg A Precede le seg B                                     */
/**************************************************************/

void Contains(double *H, double x, double y, double z, double Y0, int Case, double Poids)
{
  if (Y0<=x+y)
    H[Case] += (Y0*Y0 * log((y+x)/(x+y+z))+2*Y0*z)*Poids;
  else
    if ((y+x<=Y0)&&(Y0<=x+y+z))
      H[Case] += (Y0*Y0 * log(Y0/(x+y+z))+2*Y0*z-(x+y-3*Y0)
		      *(y+x-Y0)/2)*Poids;
    else
      /*if (x+y+z<=Y0)*/
      H[Case] += (z/2+y+x)*z*Poids;
}


/**************************************************************/
/* Seg A Precede le seg B                                     */
/**************************************************************/

void During(double *H, double x, double y, double z, double Y0, int Case, double Poids)
{
  if (Y0<=z+y)
    H[Case] += (Y0*Y0 * log((y+z)/(x+y+z))+2*Y0*x)*Poids;
  else
    if ((y+z<=Y0)&&(Y0<=x+y+z))
      H[Case] += (Y0*Y0 * log(Y0/(x+y+z))+2*Y0*x-(z+y-3*Y0)*
		      (y+z-Y0)/2)*Poids;
    else
      /*if (x+y+z<=Y0)*/
      H[Case] += (x/2+y+z)*x*Poids;
}


/**************************************************************/
/* Creation de l'histogramme (fct ln) - Hypoth H2 decompose - */
/**************************************************************/

void H2(double *Histo, struct Segment *List_Seg_A, struct Segment *List_Seg_B, int Case1, int Case2, double Poids, double Y0, double *Sum_LN_C1, double *Sum_LN_C2)
{
  static double x,y,z;
  //static int Case;
  struct Segment *Seg_A, *Seg_B, *pt_L_A, *pt_L_B;

  pt_L_A=List_Seg_A;
  
  /* Attention a gerer avec les angles apres */
  while (pt_L_A)
    {
      /* premier segment */
      Seg_A=pt_L_A;

      pt_L_B=List_Seg_B;
      while (pt_L_B)
	{          
	  Seg_B=pt_L_B;
	  /* d'abord Case 1 ... Optimisable */
	  if (Seg_A->x1 <= Seg_B->x2) 
	    { 
	      if (Seg_A->x2 < Seg_B->x1) 
		{ 
		  x=val_abs(Seg_A->x2-Seg_A->x1)+1; 
		  z=val_abs(Seg_B->x2-Seg_B->x1)+1; 
		  y=val_abs(Seg_B->x1-Seg_A->x2)-1; 
		  Before(Histo, x, y, z, Y0, Case1, Poids, Sum_LN_C1 ); 
		} 
	      else  
		if ((Seg_A->x2<Seg_B->x2)&&(Seg_A->x1<Seg_B->x1)) 
		  { 
		    x=val_abs(Seg_A->x2-Seg_A->x1)+1; 
		    z=val_abs(Seg_B->x2-Seg_B->x1)+1; 
		    y=-(val_abs(Seg_A->x2-Seg_B->x1)+1); 
		    Overlaps(Histo, x, y, z, Y0, Case1, Poids); 
		  } 
		else 
		  if ((Seg_B->x1>Seg_A->x1)&&(Seg_A->x2>=Seg_B->x2)) 
		    { 
		      x=val_abs(Seg_B->x2-Seg_A->x1)+1; 
		      z=val_abs(Seg_B->x2-Seg_B->x1)+1; 
		      y=-z; 
		      Contains(Histo, x, y, z, Y0, Case1, Poids); 
		    } 
		  else 
		    if ((Seg_B->x1<=Seg_A->x1)&&(Seg_B->x2>Seg_A->x2)) 
		      { 
			x=val_abs(Seg_A->x2-Seg_A->x1)+1; 
			z=val_abs(Seg_B->x2-Seg_A->x1)+1; 
			y=-x; 
			During(Histo, x, y, z, Y0, Case1, Poids); 
		      }  
		    else 
		      { 
			x=z=val_abs(Seg_B->x2-Seg_A->x1)+1; 
			y=-x; 
			Overlapped_By(Histo, x, y, z, Y0, Case1, Poids); 
		      } 
	    } 
	  
	  /* droite dans l'autre sens attention au = */
	  if (Seg_B->x1 <= Seg_A->x2)
	    {
	      if (Seg_B->x2 < Seg_A->x1)
		{
		  x=val_abs(Seg_A->x2-Seg_A->x1)+1;
		  z=val_abs(Seg_B->x2-Seg_B->x1)+1;
		  y=val_abs(Seg_A->x1-Seg_B->x2)-1;
		  Before(Histo, x, y, z, Y0, Case2, Poids, Sum_LN_C2);
		}
	      else
		if ((Seg_A->x2>Seg_B->x2)&&(Seg_A->x1>Seg_B->x1))
		  {
		    x=val_abs(Seg_A->x2-Seg_A->x1)+1;
		    z=val_abs(Seg_B->x2-Seg_B->x1)+1;
		    y=-(val_abs(Seg_B->x2-Seg_A->x1)+1);
		    Overlaps(Histo, x, y, z, Y0, Case2, Poids);
		  }
		else
		  if ((Seg_A->x1>Seg_B->x1)&&(Seg_B->x2>=Seg_A->x2))
		    {
		      x=val_abs(Seg_A->x2-Seg_A->x1)+1;
		      z=val_abs(Seg_A->x2-Seg_B->x1)+1;
		      y=-x;
		      During(Histo, x, y, z, Y0, Case2, Poids);
		    } 
		  else
		    if ((Seg_A->x1<=Seg_B->x1)&&(Seg_A->x2>Seg_B->x2))
		      {
			z=val_abs(Seg_B->x2-Seg_B->x1)+1;
			x=val_abs(Seg_A->x2-Seg_B->x1)+1;
			y=-z;
			Contains(Histo, x, y, z, Y0, Case2, Poids);
		      }
		    else
		      {
			x=z=val_abs(Seg_A->x2-Seg_B->x1)+1;
			y=-z;
			Overlapped_By(Histo, x, y, z, Y0, Case2, Poids);
		      }
	    }
	     
	  pt_L_B=pt_L_B->suivant; 
	}
      pt_L_A=pt_L_A->suivant;
    } 
}


/**************************************************************/
/* Creation de l'histogramme          - Hypoth H1 decompose - */
/**************************************************************/

void H1(double *Histo, struct Segment *List_Seg_A, struct Segment *List_Seg_B, int Case1, int Case2, double Poids)
{
  static double x,y,z;
  //static int Case;
  struct Segment *Seg_A, *Seg_B, *pt_L_A, *pt_L_B;

  pt_L_A=List_Seg_A;
  
  /* Attention a gere avec les angles apres */
  while (pt_L_A)
    {
      Seg_A=pt_L_A;
      /* premier segment */
      pt_L_B=List_Seg_B;
      while (pt_L_B)
	{          
	  Seg_B=pt_L_B;
	  /* d'abord Case 1 ... Optimisable */
	  if (Seg_A->x1 <= Seg_B->x2) 
	    { 
	      if (Seg_A->x2 < Seg_B->x1) 
		{ 
		  x=val_abs(Seg_A->x2-Seg_A->x1)+1; 
		  z=val_abs(Seg_B->x2-Seg_B->x1)+1; 
		  Histo[Case1]+=Poids*x*z; 
		} 
	      else  
		if ((Seg_A->x2<Seg_B->x2)&&(Seg_A->x1<Seg_B->x1)) 
		  { 
		    x=val_abs(Seg_A->x2-Seg_A->x1)+1; 
		    z=val_abs(Seg_B->x2-Seg_B->x1)+1; 
		    y=-(val_abs(Seg_A->x2-Seg_B->x1)+1); 
		    Histo[Case1]+=Poids*(x*z-y*y/2); 
		  } 
		else 
		  if ((Seg_B->x1>Seg_A->x1)&&(Seg_A->x2>=Seg_B->x2)) 
		    { 
		      x=val_abs(Seg_B->x2-Seg_A->x1)+1; 
		      z=val_abs(Seg_B->x2-Seg_B->x1)+1; 
		      y=-z; 
		      Histo[Case1]+=Poids*(x+y+z/2)*z; 
		    } 
		  else 
		    if ((Seg_B->x1<=Seg_A->x1)&&(Seg_B->x2>Seg_A->x2)) 
		      { 
			x=val_abs(Seg_A->x2-Seg_A->x1)+1; 
			z=val_abs(Seg_B->x2-Seg_A->x1)+1; 
			y=-x; 
			Histo[Case1]+=Poids*(x/2+y+z)*x; 
		      }  
		    else 
		      { 
			x=z=val_abs(Seg_B->x2-Seg_A->x1)+1; 
			y=-x; 
			Histo[Case1]+=Poids*(x+y+z)*(x+y+z)/2; 
		      }
	    } 
	  
	  /* droite dans l'autre sens attention au = */
	  if (Seg_B->x1 <= Seg_A->x2)
	    {
	      if (Seg_B->x2 < Seg_A->x1)
		{
		  x=val_abs(Seg_A->x2-Seg_A->x1)+1;
		  z=val_abs(Seg_B->x2-Seg_B->x1)+1;
		  Histo[Case2]+=Poids*x*z;
		}
	      else
		if ((Seg_A->x2>Seg_B->x2)&&(Seg_A->x1>Seg_B->x1))
		  {
		    x=val_abs(Seg_A->x2-Seg_A->x1)+1;
		    z=val_abs(Seg_B->x2-Seg_B->x1)+1;
		    y=-(val_abs(Seg_B->x2-Seg_A->x1)+1);
		    Histo[Case2]+=Poids*(x*z-y*y/2);
		  }
		else
		  if ((Seg_A->x1>Seg_B->x1)&&(Seg_B->x2>=Seg_A->x2))
		    {
		      x=val_abs(Seg_A->x2-Seg_A->x1)+1;
		      z=val_abs(Seg_A->x2-Seg_B->x1)+1;
		      y=-x;
		      Histo[Case2]+=Poids*(x/2+y+z)*x;
		    } 
		  else
		    if ((Seg_A->x1<=Seg_B->x1)&&(Seg_A->x2>Seg_B->x2))
		      {
			z=val_abs(Seg_B->x2-Seg_B->x1)+1;
			x=val_abs(Seg_A->x2-Seg_B->x1)+1;
			y=-z;
			Histo[Case2]+=Poids*(x+y+z/2)*z;
		      }
		    else
		      {
			x=z=val_abs(Seg_A->x2-Seg_B->x1)+1;
			y=-z;
			Histo[Case2]+=Poids*(x+y+z)*(x+y+z)/2;
		      }
	    }
	  pt_L_B=pt_L_B->suivant; 	     
	}
      pt_L_A=pt_L_A->suivant; 
    } 
}


/**************************************************************/
/* H2 cas floue                                               */
/**************************************************************/

void H2_Floue(double *Histo, struct Segment *List_Seg_A, struct Segment *List_Seg_B, int Case1, int Case2, double *Tab, double Y0, double *Sum_LN_C1, double *Sum_LN_C2)
{
  static double x,z,y;
  
  //static int Case;
  
  struct Segment *Seg_A, *Seg_B, *pt_L_A, *pt_L_B;

  pt_L_A=List_Seg_A;

  /* Attention a gere avec les angles apres */
  while (pt_L_A)
    {
      Seg_A=pt_L_A;
      /* premier segment */
      pt_L_B=List_Seg_B;
      while (pt_L_B)
	{         
	  Seg_B=pt_L_B;
	  /* d'abord => Pas de chevauchement */
	  if (Seg_A->x1 <= Seg_B->x1) 
	    if (Seg_A->x2 < Seg_B->x1) 
	      { 
		y=val_abs(Seg_B->x1-Seg_A->x2)-1; 
		if (Y0<=y)  
		  /*(*Sum_LN_C1) +=minimum(Seg_A->Val,Seg_B->Val)*Tab[(int)y]; */
		  Histo[Case1]+=Y0*Y0*minimum(Seg_A->Val,Seg_B->Val)*Tab[(int)y]; 
	       	else 
		  { 
		    x=1;z=1; 
		    Before(Histo, x, y, z, Y0, Case1,
			   minimum(Seg_A->Val,Seg_B->Val), Sum_LN_C1); 
		  } 
		/* Temporaire
		//x=1;z=1;Histo[Case1]+=minimum(Seg_A->Val,Seg_B->Val)*x*z; */
	      } 
	    else 
	      { 
		x=z=1;y=-1;     
  
		Overlapped_By(Histo, x, y, z, Y0, Case1, minimum(Seg_A->Val,Seg_B->Val)); 
		Overlapped_By(Histo, x, y, z, Y0, Case2, minimum(Seg_A->Val,Seg_B->Val)); 
		/* Histo[Case1]+=minimum(Seg_A->Val,Seg_B->Val)*(x+y+z)*
		  (x+y+z)/2; 
		Histo[Case2]+=minimum(Seg_A->Val,Seg_B->Val)*(x+y+z)* 
		  (x+y+z)/2; */
	      } 
	  else  
	    if (Seg_B->x2 <= Seg_A->x1) 
	      { 
		y=val_abs(Seg_B->x1-Seg_A->x2)-1; 
		if (Y0<=y)  
 		  (*Sum_LN_C2)+=minimum(Seg_A->Val,Seg_B->Val)*Tab[(int)y];  
		/* Histo[Case2]+=Y0*Y0*minimum(Seg_A->Val,Seg_B->Val)*Tab[(int)y]; */
	       	else 
		  { 
		    x=1;z=1; 
		    Before(Histo, x, y, z, Y0, Case2, minimum(Seg_A->Val,Seg_B->Val), Sum_LN_C2); 
		  } 
		/*Temporaire
		// x=1;z;Histo[Case2]+=minimum(Seg_A->Val,Seg_B->Val)*x*z; */
	      } 
	    else 
	      { 
		/* cout << " on ne doit passer la... " << endl; */
		x=z=1;y=-1;
		Overlapped_By(Histo, x, y, z, Y0, Case1, minimum(Seg_A->Val,Seg_B->Val)); 
		Overlapped_By(Histo, x, y, z, Y0, Case2, minimum(Seg_A->Val,Seg_B->Val));
		/* Histo[Case1]+=minimum(Seg_A->Val,Seg_B->Val)*(x+y+z)*
		  (x+y+z)/2; 
		Histo[Case2]+=minimum(Seg_A->Val,Seg_B->Val)*(x+y+z)*(x+y+z)/2;*/
	      } 
	  pt_L_B=pt_L_B->suivant; 	     
	}
      pt_L_A=pt_L_A->suivant; 
    } 
} 

 
/**************************************************************/
/********* Choix de la methode a appliquer ********************/ 
/**************************************************************/ 
 
void Choix_Methode(double *Histo, struct Segment *List_Seg_A, struct Segment *List_Seg_B, int Case1, int Case2, int methode, double Poids, double divi, double Y0, double *Sum_LN_C1, double *Sum_LN_C2) 
{ 

  switch (methode) 
    { 
    case 1:  
      H1_Simple(Histo, List_Seg_A, List_Seg_B, Case1, Case2); 
      break; 
    case 2: 
      H2_Simple(Histo, List_Seg_A, List_Seg_B, Case1, Case2, divi); 
      break; 
    case 3: 
      H2(Histo, List_Seg_A, List_Seg_B, Case1, Case2, 1.0, Y0, Sum_LN_C1, Sum_LN_C2); 
      break; 
    case 7: 
      H1(Histo, List_Seg_A, List_Seg_B, Case1, Case2, 1.0); 
      break; 
    } 
} 


/**************************************************************/ 
/* Prise en consideration de l'angle       ********************/ 
/**************************************************************/

void Angle_Histo(double *Histo, int Taille, int methode)
{
  int case_dep,case_op, case_dep_neg, case_op_neg;
  double angle=0;
  double Pas_Angle=2*PI/Taille;
  case_dep=case_dep_neg=Taille/2;
  case_op=0;
  case_op_neg=Taille;
  angle=Pas_Angle;
  /* Case concernee */
  case_dep++;
  case_op++;
  case_dep_neg--;
  case_op_neg--;
  if ((methode==1)||(methode==2))
    {
      while (angle<PI/4+0.0001)
	{
	  Histo[case_dep]/=cos(angle);
	  Histo[case_op]/=cos(angle);
	  Histo[case_dep_neg]/=cos(angle);
	  Histo[case_op_neg]/=cos(angle);
	  angle+=Pas_Angle;
	  case_dep++;
	  case_op++;
	  case_dep_neg--;
	  case_op_neg--;
	}
      while (angle<PI/2)
	{
	  Histo[case_dep]/=sin(angle);
	  Histo[case_op]/=sin(angle);
	  Histo[case_dep_neg]/=sin(angle);
	  Histo[case_op_neg]/=sin(angle);
	  angle+=Pas_Angle;
	  case_dep++;
	  case_op++;
	  case_dep_neg--;
	  case_op_neg--;
	}
    }
  else
    {
      while (angle<-PI/4)
	{
	  Histo[case_dep]*=cos(angle);
	  Histo[case_op]*=cos(angle);
	  Histo[case_dep_neg]*=cos(angle);
	  Histo[case_op_neg]*=cos(angle);
	  angle+=Pas_Angle;
	  case_dep++;
	  case_op++;
	  case_dep_neg--;
	  case_op_neg--;
	}
      while (angle<PI/2)
	{
	  Histo[case_dep]*=sin(angle);
	  Histo[case_op]*=sin(angle);
	  Histo[case_dep_neg]*=sin(angle);
	  Histo[case_op_neg]*=sin(angle);
	  angle+=Pas_Angle;
	  case_dep++;
	  case_op++;	
	  case_dep_neg--;
	  case_op_neg--;  
	}
    }
}


/*****************************************************/
/* Determination des segments suivant la methode     */
/* Suivant droite en X                               */
/* pour plus de proprete : il faudrait faire une     */
/* fonction free (idem pour fct suivante)...         */
/*****************************************************/

void Calcul_Seg(struct Adresse *Tab_A, struct Adresse *Tab_B, double *Histo, int methode, int x1, int y1, int pas_x, int pas_y, int Xsize, int Ysize, int case_dep, int case_op, int *Chaine,	int deb_chaine, int Cut[255], double *Tab_ln, double divi, double l, double *Sum_LN_C1, double *Sum_LN_C2)
{

  int i,j,Prec_Cut, Prec_Cut_A, Prec_Cut_B; 
  struct Segment *List_Seg_A = NULL, *List_Seg_B = NULL;

  switch (methode)
    {

      /* double sigma pixel a pixel */
    case 4: /* free a realiser ds le cas classique */
      /* double sommation : cas pixel */
      List_Seg_A=ligne_x_floue(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
				List_Seg_A, pas_x, pas_y);
      List_Seg_B=ligne_x_floue(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
				List_Seg_B, pas_x, pas_y);
      if (List_Seg_A&&List_Seg_B)
	H2_Floue(Histo, List_Seg_A, List_Seg_B, case_dep, case_op, Tab_ln, l,
		 Sum_LN_C1, Sum_LN_C2);
      break;

      /* Simple sommation cas flou */
    case 5:
      /* optimisable non trivial - gestion des segments */
      /* simple sommation (Style Khrisnapuram) - segment */
      i=1; Prec_Cut = 0;
      while (i<=Cut[0])
	{
	  List_Seg_A=ligne_x(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			      List_Seg_A, pas_x, pas_y, Cut[i]);
	  List_Seg_B=ligne_x(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
			      List_Seg_B, pas_x, pas_y, Cut[i]);
	  if (List_Seg_A&&List_Seg_B)
	    {
	      H2(Histo, List_Seg_A, List_Seg_B, case_dep, case_op,
		 (double) (Cut[i]-Prec_Cut)/255.0, l, Sum_LN_C1, Sum_LN_C2) ;
	      Prec_Cut = Cut[i];
	      i++;
	    }
	  else i=Cut[0]+1;
	}
      break;
      
	  /* double sommation segment a segment */
    case 6:
      /* optimisable mais non trivial */
      /* double sommation (Style Dubois) - segment */
      i=1; Prec_Cut_A = 0;
      while (i<=Cut[0])
	{
	  j=1; Prec_Cut_B = 0;
	  List_Seg_A=ligne_x(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			      List_Seg_A, pas_x, pas_y, Cut[i]);
	  while (j<=Cut[0])
	    {
	      List_Seg_B=ligne_x(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
				  List_Seg_B, pas_x, pas_y, Cut[j]);
	      if (List_Seg_A&&List_Seg_B)
		{
		  H2(Histo, List_Seg_A, List_Seg_B, case_dep, case_op,
		     ((double) (Cut[i]-Prec_Cut_A)/255.0)*
		     ((double) (Cut[j]-Prec_Cut_B)/255.0), l,
		     Sum_LN_C1, Sum_LN_C2);
		  Prec_Cut_B = Cut[j];
		  j++;
		}
	      else  j=Cut[0]+1;
	    }
	  if (!List_Seg_A)
	    i=Cut[0]+1;
	  else 
	    { Prec_Cut_A = Cut[i]; i++;}
	}
      break;

    case 8:
      /* optimisable non trivial - gestion des segments */
      /* simple sommation (Style Khrisnapuram) - segment */
      i=1; Prec_Cut = 0;
      while (i<=Cut[0])
	{
	  List_Seg_A=ligne_x(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			     List_Seg_A, pas_x, pas_y, Cut[i]);
	  List_Seg_B=ligne_x(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
			     List_Seg_B, pas_x, pas_y, Cut[i]);
	  if (List_Seg_A&&List_Seg_B)
	    {
	      H1(Histo, List_Seg_A, List_Seg_B, case_dep, case_op,
		 (double) (Cut[i]-Prec_Cut)/255.0) ;
	      Prec_Cut = Cut[i];
	      i++;
	    }
	  else i=Cut[0]+1;
	}
      break;
      
      /* cas classiques... */
    default:
      {
	List_Seg_A=ligne_x(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			   List_Seg_A, pas_x, pas_y, 1);
	List_Seg_B=ligne_x(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
			   List_Seg_B, pas_x, pas_y, 1);
	if (List_Seg_A&&List_Seg_B)
	  Choix_Methode(Histo, List_Seg_A, List_Seg_B, case_dep, case_op, 
			methode, 1, divi, l, Sum_LN_C1, Sum_LN_C2);
      }
    }
}



/*****************************************************/
/* Determination des segments suivant la methode     */
/* Suivant droite en Y                               */
/*****************************************************/

void Calcul_Seg_Y(struct Adresse *Tab_A, struct Adresse *Tab_B, double *Histo, int methode, int x1, int y1, int pas_x, int pas_y, int Xsize, int Ysize, int case_dep, int case_op, int *Chaine, int deb_chaine, int Cut[255], double *Tab_ln, double divi, double l, double *Sum_LN_C1, double *Sum_LN_C2)
{
  int i,j,Prec_Cut, Prec_Cut_A, Prec_Cut_B;
  struct Segment *List_Seg_A = NULL, *List_Seg_B = NULL;

  switch (methode)
    {
      /* double sigma pixel a pixel */
    case 4: /* free a realiser ds le cas classique */
      /* double sommation : cas pixel */
      List_Seg_A=ligne_y_floue(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			       List_Seg_A, pas_x, pas_y);
      List_Seg_B=ligne_y_floue(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
			       List_Seg_B, pas_x, pas_y);
      if (List_Seg_A&&List_Seg_B)
	H2_Floue(Histo, List_Seg_A, List_Seg_B, case_dep, case_op, Tab_ln, l,
		 Sum_LN_C1, Sum_LN_C2);
      break;

      /* cas flou simple sigma */
    case 5:
      /* optimisable non trivial - gestion des segments */
      /* simple sommation (Style Khrisnapuram) - segment */
      i=1; Prec_Cut = 0;
      while (i<=Cut[0])
	{
	  List_Seg_A=ligne_y(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			     List_Seg_A, pas_x, pas_y, Cut[i]);
	  List_Seg_B=ligne_y(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
			     List_Seg_B, pas_x, pas_y, Cut[i]);
	  if (List_Seg_A&&List_Seg_B)
	    {
	      H2(Histo, List_Seg_A, List_Seg_B, case_dep, case_op,
		 (double) (Cut[i]-Prec_Cut)/255.0, l, Sum_LN_C1, Sum_LN_C2);
	      Prec_Cut = Cut[i];
	      i++;
	    }
	  else i=Cut[0]+1;
	}
      break;
      
      /* cas flou double sigma */
    case 6:
      /* optimisable non trivial */
      /* double sommation (Style Dubois) - segment */
      i=1; Prec_Cut_A = 0;
      while (i<=Cut[0])
	{
	  List_Seg_A=ligne_y(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			     List_Seg_A, pas_x, pas_y, Cut[i]);
	  j=1; Prec_Cut_B = 0;
	  while (j<=Cut[0])
	    {
	      List_Seg_B=ligne_y(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
				 List_Seg_B, pas_x, pas_y, Cut[j]);
	      if (List_Seg_A&&List_Seg_B)
		{
		  H2(Histo, List_Seg_A, List_Seg_B, case_dep, case_op,
		     ((double) (Cut[i]-Prec_Cut_A)/255.0)*
		     ((double) (Cut[j]-Prec_Cut_B)/255.0), l,
		     Sum_LN_C1, Sum_LN_C2);
		  Prec_Cut_B = Cut[j];
		  j++;
		}
	      else j=Cut[0]+1;
	    }
	  if (!List_Seg_A)
	    i=Cut[0]+1; 
	  else 
	    { Prec_Cut_A = Cut[i]; i++;}
	}
      break;
 
   case 8:
     /* optimisable non trivial - gestion des segments */
     /* simple sommation (Style Khrisnapuram) - segment */
     i=1; Prec_Cut = 0;
     while (i<=Cut[0])
       {
	 List_Seg_A=ligne_y(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			    List_Seg_A, pas_x, pas_y, Cut[i]);
	 List_Seg_B=ligne_y(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
			    List_Seg_B, pas_x, pas_y, Cut[i]);
	 if (List_Seg_A&&List_Seg_B)
	   {
	     H1(Histo, List_Seg_A, List_Seg_B, case_dep, case_op,
		(double) (Cut[i]-Prec_Cut)/255.0) ;
	     Prec_Cut = Cut[i];
	     i++;
	   }
	 else i=Cut[0]+1;
       }
     break;
      
     /* cas classiques */
    default:
      {
	List_Seg_A=ligne_y(Tab_A, x1, y1, Xsize, Ysize, Chaine, deb_chaine, 
			   List_Seg_A, pas_x, pas_y, 1);
	List_Seg_B=ligne_y(Tab_B, x1, y1, Xsize, Ysize, Chaine, deb_chaine,
			   List_Seg_B, pas_x, pas_y, 1);
	if (List_Seg_A&&List_Seg_B) 
	  Choix_Methode(Histo, List_Seg_A, List_Seg_B, case_dep, case_op, 
			methode, 1, divi, l, Sum_LN_C1, Sum_LN_C2);
      }
    }   
}


/**************************************************************/
/*                                                            */
/*             Methode generale :                             */
/*             ------------------                             */
/*                                                            */
/*     Calcul des segments suivant tous les angles possibles  */
/*     Puis application du type de methode selectionner       */
/*     1 : Hypothese H1 (Miyajima)                            */
/*     2 : Hypothese H2 (Fonction ln simple)                  */
/*     3 : Hypothese H2 (tous les cas possibles)              */
/*     4 : Hypothese H2 (Cas flou simple pixel & double Sum)  */
/*     5 : Hupothese H2 (Cas flou Segment Simple Sum)         */ 
/*     6 : Hupothese H2 (Cas flou Segment Double Sum)         */ 
/*     7 : Hupothese H1 (Fonction simple)                     */ 
/*     6 : Hupothese H1 (Cas flou Segment Simple Sum)         */ 
/*                                                            */
/**************************************************************/  
                
void Our_Methode(DATA *Image_A, DATA *Image_B, int Xsize, int Ysize, int Taille, double *Histo, int methode, int Cut[255], double l)
{
  /* Structure pour l'image */
  //FILE *f;
  int j;
  struct Adresse *Tab_A, *Tab_B;
  double *tab_ln;
  double nbcouple;
  int *Chaine;
  int deb_chaine, case_dep, case_dep_neg, case_op, case_op_neg; 
  int Taille_PI_SUR_2;
  int x1,x2,y1,y2; 
  int i;
  double Sum_LN_C1,Sum_LN_C2; 
  double angle, max; 
  double Pas_Angle;

  Tab_A=(struct Adresse *)malloc(Ysize*sizeof(struct Adresse)); 
  Tab_B=(struct Adresse *)malloc(Ysize*sizeof(struct Adresse)); 
 
  Cree_Tab_Pointeur(Image_A, Tab_A, Xsize, Ysize); 
  Cree_Tab_Pointeur(Image_B, Tab_B, Xsize, Ysize); 

  /* Tableau de ln constante */
  tab_ln=(double *)malloc(Xsize*sizeof(double));
 
  Cree_Tab_ln(tab_ln, Xsize); 

  // Bart Lamiroy bug correction for cases of non square images
  Chaine=(int *)malloc((2*((Xsize > Ysize) ? Xsize : Ysize)+1)*sizeof(int));

  Taille_PI_SUR_2 = (int) Taille/4; 

  Pas_Angle = 2*PI/Taille; /* PI/(Taille+1) */

  /************* Angle = 0 *****************/
  angle=0;
  x1=y1=y2=0; 
  x2=Xsize; 
  Bresenham_X(x1, y1, x2, y2, Xsize, Chaine); 
  case_dep=Taille/2; 
  case_op=0; 
  case_dep_neg=Taille/2; 
  case_op_neg=Taille; 
  deb_chaine=1; 

  Sum_LN_C1=Sum_LN_C2=0; 
 
  for (y1=0;y1<Ysize;y1++) 
    Calcul_Seg(Tab_A, Tab_B, Histo, methode, x1, y1, 1, 1, Xsize, Ysize,  
	       case_dep, case_op, Chaine, deb_chaine, Cut, tab_ln, 1, l, 
	       &Sum_LN_C1,&Sum_LN_C2); 

  if (methode>2) 
    { 
      Histo[case_dep] = Histo[case_dep]/(l*l) + Sum_LN_C1; 
      Histo[case_op]  = Histo[case_op]/(l*l) + Sum_LN_C2; 
    }

  /********** angle in ]-pi/4,pi/4[ ***************/ 

  angle+=Pas_Angle; 
  x2 = Xsize + 200; 
  
  while (angle<PI/4+0.0001) /* Arghhhhhh.... PI/4 + 0.0001*/
    { 
      y2 = (int) (x2 * tan (angle)); 
      x1=0; 
      y1=0; 

      case_dep++;   
      case_op++; 

      /* On determine la droite a translater... */
      Bresenham_X(x1, y1, x2, y2, Xsize, Chaine); 

      /* Vertical */
      deb_chaine=1; 

      Sum_LN_C1=Sum_LN_C2=0; 

      for (y1=0;y1<Ysize;y1++) 
	Calcul_Seg(Tab_A, Tab_B, Histo, methode, x1, y1, 1, 1, Xsize, Ysize,  
		   case_dep, case_op, Chaine, deb_chaine, Cut, tab_ln, 
		   cos(angle), l*cos(angle), &Sum_LN_C1, &Sum_LN_C2); 
     


      /* Horizontal */
      y1=0; 
      while (x1<Xsize) 
	{ 
	  x1+=Chaine[deb_chaine]; 
	  deb_chaine+=2; 
	  Calcul_Seg(Tab_A, Tab_B, Histo, methode, x1, y1, 1, 1, Xsize, Ysize,  
		     case_dep, case_op, Chaine, deb_chaine, Cut, tab_ln, 
		     cos(angle), l*cos(angle), &Sum_LN_C1, &Sum_LN_C2); 
	} 

      if (methode>2) 
	{ 
	  Histo[case_dep] = Histo[case_dep]/(cos(angle)*l*l) +  
	    Sum_LN_C1*cos(angle); 
	  Histo[case_op]  = Histo[case_op]/(cos(angle)*l*l) + 
	    Sum_LN_C2*cos(angle); 
	} 
	  
      /************* Angle negatif oppose *******/ 
      case_dep_neg--; 
      case_op_neg--; 

      /* Vertical */
      deb_chaine=1; 

      Sum_LN_C1=Sum_LN_C2=0; 
 
      x1=0; 

      /*           for (y1=0;y1<Ysize;y1++) // AR Ysize...
	 Calcul_Seg(Tab_A, Tab_B, Histo, methode, x1, y1, 1, -1, Xsize, -1, 
		    case_op_neg, case_dep_neg, Chaine, deb_chaine, Cut, 
		    tab_ln, cos(angle), l*cos(angle), &Sum_LN_C1, &Sum_LN_C2);
    
       // Horizontal
       deb_chaine=1;   
       y1=Ysize-1; 
	
       while (x1<Xsize)
	{ 
	   x1+=Chaine[deb_chaine];
	   deb_chaine+=2;
	   Calcul_Seg(Tab_A, Tab_B, Histo, methode, x1, y1, 1, -1, -1, Ysize, 
		      case_op_neg, case_dep_neg, Chaine, deb_chaine, Cut, 
		      tab_ln, cos(angle), l*cos(angle), &Sum_LN_C1, &Sum_LN_C2);

	} 
*/
 
	if (case_dep_neg >= (Taille/2-Taille/8)) 
	{ 
          x1=0; 
          for (y1=0;y1<Ysize;y1++) /* AR Ysize... */
	    Calcul_Seg(Tab_A, Tab_B, Histo, methode, x1, y1, 1, -1, Xsize, -1,  
		    case_dep_neg, case_op_neg, Chaine, deb_chaine, Cut,  
		    tab_ln, cos(angle), l*cos(angle), &Sum_LN_C1, &Sum_LN_C2); 
    
	  /* Horizontal */
         deb_chaine=1;    
         y1=Ysize-1;  
         while (x1<Xsize) 
	  { 
	    x1+=Chaine[deb_chaine]; 
	    deb_chaine+=2; 
	    Calcul_Seg(Tab_A, Tab_B, Histo, methode, x1, y1, 1, -1, Xsize, -1,  
		     case_dep_neg, case_op_neg, Chaine, deb_chaine, Cut,  
		     tab_ln, cos(angle), l*cos(angle), &Sum_LN_C1, &Sum_LN_C2); 

	  } 

         if (methode>2) 
	  { 
	    Histo[case_dep_neg] = Histo[case_dep_neg]/(cos(angle)*l*l) +  
	      Sum_LN_C1*cos(angle); 
	    Histo[case_op_neg]  = Histo[case_op_neg]/(cos(angle)*l*l) +  
	      Sum_LN_C2*cos(angle); 
	  } 
      
          angle+=Pas_Angle; 
         } 
      else 
        { 
           x1=0;
           deb_chaine=1;    
        
           for (y1=0;y1<Ysize;y1++) /* AR Ysize...*/
	     Calcul_Seg(Tab_A, Tab_B, Histo, methode, x1, y1, 1, -1, Xsize, -1,  case_dep_neg,
			case_op_neg, Chaine, deb_chaine, Cut, tab_ln, cos(angle), 
			l*cos(angle), &Sum_LN_C1, &Sum_LN_C2); 
  
	   /* Horizontal */
          deb_chaine=1;    
          y1=Ysize-1;  
            while (x1<Xsize) 
	   { 
	     x1+=Chaine[deb_chaine]; 
	     deb_chaine+=2; 
	     Calcul_Seg   (Tab_A, Tab_B, Histo, methode, x1, y1, 1, -1, Xsize, -1,  
		     case_dep_neg, case_op_neg, Chaine, deb_chaine, Cut,  
		     tab_ln, sin(angle), l*sin(angle), &Sum_LN_C1, &Sum_LN_C2); 

	   } 

          if (methode>2) 
	  {
	    Histo[case_dep_neg] = Histo[case_dep_neg]/(cos(angle)*l*l) +  
	      Sum_LN_C1*cos(angle); 
	    Histo[case_op_neg]  = Histo[case_op_neg]/(cos(angle)*l*l) +  
	      Sum_LN_C2*cos(angle); 
	  } 
	  angle+=Pas_Angle; 
      } 
} 
  /*********** angle in [PI/4, PI/2[ ***************/ 
  /*** projection suivant l'axe des Y... ***********/ 
  /********** angle in ]0,pi/4[ ***************/ 

  while (angle<PI/2)
    {
      y2 = (int) (x2 * tan (angle));
      x1=0; 
      y1=0;
      case_dep++;      
      case_op++;      

      /* On determine la droite a translater... */
      Bresenham_Y(x1, y1, x2, y2, Ysize, Chaine);

      // Horizontal 
      Sum_LN_C1=Sum_LN_C2=0;

      deb_chaine=1;
      for (x1=0;x1<Xsize;x1++)
	Calcul_Seg_Y(Tab_A, Tab_B, Histo, methode, x1, y1, 1, 1, Xsize, 
		     Ysize, case_dep, case_op, Chaine, deb_chaine, Cut, tab_ln,
		     sin(angle), l*sin(angle), &Sum_LN_C1, &Sum_LN_C2);
    
      /* Vertical */
      x1=0;
      y1=0;
      while (y1<Ysize)
	{
	  y1+=Chaine[deb_chaine];
	  deb_chaine+=2;
	  Calcul_Seg_Y(Tab_A, Tab_B, Histo, methode, x1, y1, 1, 1, Xsize,Ysize,
		       case_dep, case_op, Chaine, deb_chaine, Cut, tab_ln,
		       sin(angle), l*sin(angle), &Sum_LN_C1, &Sum_LN_C2);
	}

      if (methode>2)
	{
	  Histo[case_dep] = Histo[case_dep]/(sin(angle)*l*l) + 
	    Sum_LN_C1*sin(angle);
	  Histo[case_op]  = Histo[case_op]/(sin(angle)*l*l) + 
	    Sum_LN_C2*sin(angle);
	}

      /******** Partie oppose ***************/
      case_dep_neg--;
      case_op_neg--;

      /* Horizontal */
      Sum_LN_C1=Sum_LN_C2=0;
      deb_chaine=1;
      y1=0;  //  y1=Ysize-1 est envisageable aussi...

      for (x1=0;x1<Xsize;x1++)
	Calcul_Seg_Y(Tab_A, Tab_B, Histo, methode, x1, y1, -1, 1, -1, Ysize, 
		     case_op_neg, case_dep_neg, Chaine, deb_chaine,Cut, tab_ln,
		     sin(angle), l*sin(angle), &Sum_LN_C2, &Sum_LN_C1);

      /* Vertical */
      x1=Xsize-1;
      y1=0;
      while (y1<Ysize)
	{
	  y1+=Chaine[deb_chaine];
	  deb_chaine+=2;
	  Calcul_Seg_Y(Tab_A, Tab_B, Histo, methode, x1, y1, -1, 1, -1, Ysize,
		       case_op_neg, case_dep_neg,Chaine,deb_chaine,Cut, tab_ln,
		       sin(angle), l*sin(angle), &Sum_LN_C2, &Sum_LN_C1);
	}

      if (methode>2)
	{
	  Histo[case_dep_neg] = Histo[case_dep_neg]/(sin(angle)*l*l) +
	    Sum_LN_C1*sin(angle);
	  Histo[case_op_neg]  = Histo[case_op_neg]/(sin(angle)*l*l) +
	    Sum_LN_C2*sin(angle);
	}
	
      angle+=Pas_Angle;
    }
    
  /************* Angle = PI/2 *****************/
  y1=x1=x2=0;
  y2=Ysize;
  case_dep++;      
  case_op++;      
  Bresenham_Y(x1, y1, x2, y2, Ysize, Chaine);
  
  deb_chaine=1;
  Sum_LN_C1=Sum_LN_C2=0;

  for (x1=0;x1<Xsize;x1++)
    Calcul_Seg_Y(Tab_A, Tab_B, Histo, methode, x1, y1, 1, 1, Xsize, 
		 Ysize, case_dep, case_op, Chaine, deb_chaine,
		 Cut, tab_ln, 1, l, &Sum_LN_C1, &Sum_LN_C2);

  if (methode>2)
    {
      Histo[case_dep] = Histo[case_dep]/(l*l) + Sum_LN_C1;
      Histo[case_op]  = Histo[case_op]/(l*l) + Sum_LN_C2;
    }

  // Atribution de la valeur associee a -PI
  Histo[Taille] += Histo[0];
  Histo[0] = Histo[Taille];

  if (methode<3)
    {
      nbcouple=0.0;
      for(i=0;i<Taille;i++) nbcouple+=Histo[i];
      // printf("Nombre de Couple : %d \n", (int)nbcouple);
      Angle_Histo(Histo, Taille, methode);
    }

  max = 0;
  for (j=0;j<=Taille;j++)
    if (Histo[j]>max)
      max=Histo[j];

 /* if(!(f=fopen("H2","wb"))) printf("erreur\n");
  fprintf(f,"l1:=[");
  for (j=0;j<Taille;j++)
    fprintf(f,"[%d,%f],",j,Histo[j]/max);
  fprintf(f,"[%d,%f]]:",j,Histo[j]/max);

  fclose(f);

  fclose(f);*/
} 



