/**********************************************/
/* Lecture et ecriture d'une image sur disque */
/**********************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Structure.h"

/*extern int system(char *);*/


/*************************************************/
/* Ecriture de l'histogramme                     */
/*************************************************/

void Ecriture_Histo(char *Nom_Histo, double *Histo, int Taille)
{
   FILE *lpFile;
   int i;

   lpFile = fopen(Nom_Histo,"wt") ;

   for (i=0;i<=Taille;i++)
     fprintf(lpFile,"%f %c", Histo[i], ' ');
	
   fclose(lpFile);
}


/*************************************************/
/* Lecture d'une image                           */
/*************************************************/

int readpicture( DATA *picture, int Xsize, int Ysize, char *filename)
{
  int i;
  FILE *fimage;

  if( (fimage= fopen( filename, "rb")) == NULL)
    return( -1);

  for( i=0; i<Ysize; i++)
    if( !fread( (char *)picture+Xsize*i, Xsize*sizeof(DATA), 1, fimage))
      return( -2);

  fclose( fimage);
  return( 1);
}


/*************************************************/
/* Ecriture d'une image                          */
/*************************************************/

int writepicture( char *filename, DATA *picture, int Xsize, int Ysize)
{
  int i;
  FILE *fimage;

  if( (fimage= fopen( filename, "wb")) == NULL)
    return( -1);

  for( i=0; i<Ysize; i++)
    if( !fwrite( (char *)picture+Xsize*i, Xsize*sizeof(DATA), 1, fimage))
      return( -2);

  fclose( fimage);
  return( 1);
}


/*inline*/ char *itoa2( int val)
{ 
  char *str;
  str=(char *)malloc(10);
  sprintf( str, "%d", val);
  return( str);
}


int displayimage( DATA *picture, int Xsize, int Ysize,
		  DATA cutmin, DATA cutmax,
                  char *message)
{
  int i;
  float dyn;
  FILE *fimage;
  int nbpix;
  char *xvcommand;
  unsigned char *picture8bit;

  xvcommand=(char *)malloc(255);
  picture8bit=(unsigned char *)malloc(Xsize*Ysize);

  /* creation de l'image sur disque */
  if( (fimage= fopen( "imadisp", "wb")) == NULL)
    return( -1);

  /* Conversion en 8 bits */
  dyn= 255.0/(float)(cutmax-cutmin);
  for( i=0; i<Xsize*Ysize; i++)
  { if( *(picture+i) <= cutmin)  *(picture8bit+i)= 0;
    else if( *(picture+i) >= cutmax)  *(picture8bit+i)= 255;
         else *(picture8bit+i)= (unsigned char)
                                ((float)(*(picture+i)-cutmin)*dyn);
  }

  for( i=0; i<Ysize; i++)
    if( !fwrite( (char *)picture8bit+Xsize*i, Xsize, 1, fimage))
      return( -2);
  fclose( fimage);

  printf( "Affichage : %s\n", message);
  printf( "X size ...... = %d\n", Xsize);
  printf( "Y size ...... = %d\n", Ysize);
  nbpix= Xsize * Ysize;
  printf( "Image size .. = %d\n", nbpix);
  printf( "Bits/pix. ... = %d\n", sizeof( DATA)*8);
  printf( "Seuil minimum = %d\n", cutmin);
  printf( "Seuil maximum = %d\n\n", cutmax);

  /* affichage de l'image par xv */
  strcpy( xvcommand, "/home/elsa2/xv/xv-3.00/xv -quit -brut ");
  strcat( xvcommand, itoa2( Xsize));
  strcat( xvcommand, " ");
  strcat( xvcommand, itoa2( Ysize));
  strcat( xvcommand, " imadisp");
  system( xvcommand);
  return( 1);
}

int displayimage2( unsigned short *picture, int Xsize, int Ysize,
		  unsigned short cutmin, unsigned short cutmax,
                  char *message)
{
  int i;
  float dyn;
  FILE *fimage;
  int nbpix;
  char *xvcommand;
  unsigned char *picture8bit;

  xvcommand=(char *)malloc(255);
  picture8bit=(unsigned char *)malloc(Xsize*Ysize);

  /* creation de l'image sur disque */
  if( (fimage= fopen( "imadisp", "wb")) == NULL)
    return( -1);

  /* Conversion en 8 bits */
  dyn= 255.0/(float)(cutmax-cutmin);
  for( i=0; i<Xsize*Ysize; i++)
  { if( *(picture+i) <= cutmin)  *(picture8bit+i)= 0;
    else if( *(picture+i) >= cutmax)  *(picture8bit+i)= 255;
         else *(picture8bit+i)= (unsigned char)
                                ((float)(*(picture+i)-cutmin)*dyn);
  }

  for( i=0; i<Ysize; i++)
    if( !fwrite( (char *)picture8bit+Xsize*i, Xsize, 1, fimage))
      return( -2);
  fclose( fimage);

  printf( "Affichage : %s\n", message);
  printf( "X size ...... = %d\n", Xsize);
  printf( "Y size ...... = %d\n", Ysize);
  nbpix= Xsize * Ysize;
  printf( "Image size .. = %d\n", nbpix);
  printf( "Bits/pix. ... = %d\n", sizeof( DATA)*8);
  printf( "Seuil minimum = %d\n", cutmin);
  printf( "Seuil maximum = %d\n\n", cutmax);

  /* affichage de l'image par xv */
  strcpy( xvcommand, "/home/elsa2/xv/xv-3.00/xv -quit -brut ");
  strcat( xvcommand, itoa2( Xsize));
  strcat( xvcommand, " ");
  strcat( xvcommand, itoa2( Ysize));
  strcat( xvcommand, " imadisp");
  system( xvcommand);
  return( 1);
}



int readFITS( DATA *picture, int Xsize, int Ysize, char *FITSname)
{
  int i;
  FILE *fimage;
  char *Str;
  char *creator;
  char cbuffH, cbuffL;
  //int datalen;
  //int nbpix;
  char *header; 

  Str=(char *)malloc(30);
  creator=(char *)malloc(30);
  header=(char *)malloc(8640);

  if( (fimage= fopen( FITSname, "rb")) == NULL)
    return( -1);

  printf( "Chargement de l'image : %s\n", FITSname);
  fread( header, 8640, 1, fimage);
  for( i=0; i<Ysize*Xsize; i++)
  { fread( &cbuffH, 1, 1, fimage);
    fread( &cbuffL, 1, 1, fimage);
    *(picture+i)= (DATA)((unsigned char)cbuffH*256+(unsigned char)cbuffL);
  }

  fclose( fimage);
  return( 1);
}



int readheaderFITS( int *Xsize, int *Ysize, char *FITSname)
{
  FILE *fimage;
  char *Str; 
  char *creator;
  char *date;
  char *srcname;
  int datalen;
  int nbpix;

  Str=(char *)malloc(30);
  creator=(char *)malloc(30);
  date=(char *)malloc(30);
  srcname=(char *)malloc(30);

  if( (fimage= fopen( FITSname, "rb")) == NULL)
    return( -1);

  fseek( fimage, 89, 0);
  fread( Str, 21, 1, fimage);
  Str[21]=0;
  datalen= atoi( Str);
  fseek( fimage, 249, 0);
  fread( Str, 21, 1, fimage);
  Str[21]=0;
  *Xsize= atoi( Str);
  fseek( fimage, 329, 0);
  fread( Str, 21, 1, fimage);
  Str[21]=0;
  *Ysize= atoi( Str);
  fseek( fimage, 1609, 0);
  fread( Str, 21, 1, fimage);
  Str[21]=0;
  strcpy( creator, Str);
  fseek( fimage, 1689, 0);
  fread( Str, 21, 1, fimage);
  Str[21]=0;
  strcpy( date, Str);
  fseek( fimage, 1769, 0);
  fread( Str, 21, 1, fimage);
  Str[21]=0;
  strcpy( srcname, Str);
  nbpix= *Xsize * *Ysize;
  printf( "Lecture d'un header :\n");
  printf( "File Name ... = %s\n", FITSname);
  printf( "Creator ..... = %s\n", creator);
  printf( "Date ........ = %s\n", date);
  printf( "Original file = %s\n", srcname);
  printf( "X size ...... = %d\n", *Xsize);
  printf( "Y size ...... = %d\n", *Ysize);
  printf( "Image size .. = %d\n", nbpix);
  printf( "Bits/pix. ... = %d\n\n", datalen);
  if( datalen != 16)
  { printf("Impossible de lire les donnees ! (16 bits uniquement)\n");
    return( -3);
  }
  fclose( fimage);
  return( 1);
}



void searchcuts( DATA *picture, int Xsize, int Ysize,
		 DATA *cutmin, DATA *cutmax,
                 char *message)
{ char *reponse;
  int cutmx= 0;
  reponse=(char *)malloc(20);

  do
  { displayimage( picture, Xsize, Ysize, *cutmin, *cutmax, message);
    printf( "Saisie des seuils de visualisation (RC pour finir)\n");
    do
    { printf( "Seuil minimum : ");
      fgets( reponse, 6, stdin);
      if( strcmp( reponse, "\n") != 0)   cutmx= atoi( reponse);
      else  cutmx= 0;
      if( cutmx < 0)  printf( "Le seuil doit etre positif\n");
    } while( cutmx < 0);
    if( strcmp( reponse, "\n") != 0)
    { *cutmin= cutmx;
      do
      { printf( "Seuil maximum : ");
        fgets( reponse, 6, stdin);
        if( strcmp( reponse, "\n") != 0)  cutmx= atoi( reponse);
        else  cutmx= 0;
        if( (cutmx < 0) || (cutmx < *cutmin))
          printf( "Le seuil doit etre positif et superieur au minimum\n");
      } while( (cutmx < 0) || (cutmx < *cutmin));
    }    
    if( strcmp( reponse, "\n") != 0)   *cutmax= cutmx;
    printf( "\n");
  } while( strcmp( reponse, "\n") != 0);

}



int writeFITS( char *FITSname,
               DATA *picture, int Xsize, int Ysize,
               char *FITSsource)
{
  int i;
  FILE *fimage;
  FILE *fsource;
  char *Str;
  char *creator;
  char *header;
  Str=(char *)malloc(30);
  creator=(char *)malloc(80);
  header=(char *)malloc(8640);

  if( (fsource = fopen( FITSsource, "rb")) == NULL)  return( -2);
  if( (fimage= fopen( FITSname, "wb")) == NULL)  return( -1);

  fread( header, 8640, 1, fsource);
  fclose( fsource);
  sprintf( Str, "%21d", Xsize);
  for( i=0; i<21; i++)  *(header+249+i)= *(Str+i);
  sprintf( Str, "%21d", Ysize);
  for( i=0; i<21; i++)  *(header+329+i)= *(Str+i);
  strcpy( creator, " 'Groupe TCI - IRIT'  / Written by Fuzzy Segmentation");
  for( i=0; i<53; i++)  *(header+1609+i)= *(creator+i);

  fwrite( header, 1, 8640, fimage);
  fwrite( (char *)picture, 2, Xsize*Ysize, fimage);

  fclose( fimage);
  return( 1);
}


