/****************************************************************************

		    RELATIONS SPATIALES DIRECTIONNELLES
		     (P.Matsakis/L.Wendling/J.Desachy)


     Execution de la methode Miyajima. Creation d'un histogramme d'angle
defini par tous les angles possibles entre les droites formees a partir
d'un point de chacune des deux regions et l'horizontal. 
Enfin, calcul du degre d'appartenance (Compacite entre ensembles flous obtenu
 a pres normalisation de l'histogramme d'angles et fonction d'appartenace 
sinusoidale ou trapezoidale).

=============================================================================

 Date   | avril 1996
 Auteur | L.Wendling

============================================================================*/

#include "Structure.h"
#include <stdio.h>

/* Degre / Angle et fct sinusoidale */
void Mu_Angle_Sin(double, struct Orientation *);


/* Degre / Angle et fct trapezoidale */
void Mu_Angle_Trapeze(double, struct Orientation *, double);


/* Affichage des valeurs medianes */
void Affiche_Orientation(struct Orientation *);


/* Initilisation de la structure orientation */
void Init_Orientation(struct Orientation *);


int min_int(int A, int B)
{
  if (A>B) return(B);
  else return(A);
}


/**********************************************************/
/* Calcul de l'histogramme d'angles associe a la methode  */
/* Miyajima                                               */
/**********************************************************/

void Histo_Angles(double *H, int Taille, struct TWL *L_pt_A, struct TWL *L_pt_B)
{
  double S,D,atg;
  double Inf, Sup, Inter;
  int nb;
  
  struct TWL *Elem_A;
  struct TWL *Elem_B;

  struct TWL *deb_a, *deb_b;

  Inf=Sup=Inter=nb=0;

  deb_a=L_pt_A;  deb_b=L_pt_B;
  while (deb_a)
    {
      Elem_A=deb_a;
      deb_b=L_pt_B;
      while (deb_b)
	{          
	  Elem_B=deb_b;
	  /* superposition*/
	  if ((Elem_B->Y!=Elem_A->Y)||(Elem_B->X!=Elem_A->X))
	    {
	      S=(double)(Elem_B->Y-Elem_A->Y);
	      D=(double)(Elem_B->X-Elem_A->X);
	      atg=atan2(S,D)+PI;

	      atg=((double)(Taille))*atg/(2*PI)+0.5;//+Taille/2;
		
	      nb+=min_int(Elem_B->Valeur,Elem_A->Valeur);
	      H[(int) atg]+= min_int(Elem_B->Valeur,Elem_A->Valeur);
	    }
	  deb_b=deb_b->suivant;
	}
      deb_a=deb_a->suivant;
    }
  /* Normalisation de l'histogramme.... 
  if (nb)
    for (i=0;i<=Taille;i++)
      H[i]/=nb;
  H[Taille]+=H[0];
  H[0]=H[Taille];*/
}









