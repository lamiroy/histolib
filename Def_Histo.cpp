/****************************************************************************
		    RELATIONS SPATIALES DIRECTIONNELLES
		     (P.Matsakis/L.Wendling/J.Desachy)


 Fichier Principal : lecture des donnees et execution des methodes

=============================================================================

 Date   | avril 1996
 Auteur | L.Wendling

============================================================================*/

#include "Structure.h"
#include <stdio.h>
#include <qgarlib/GenImage.h>

int supertestnul;

using namespace qgar;

/* Chainage des points des regions */
extern struct TWL *ListePoint(DATA *, struct TWL *, int, int, int [], int *);

/* Calcul de l'histo version Miyajima */
extern void Histo_Angles(double *,int, struct TWL *, struct TWL *);

/* Lecture d'une image*/
extern int readpicture(DATA *, int, int, char *);

/*  Ecriture de l'histogramme */
extern void Ecriture_Histo(char *, double *, int);

extern void Angle_Histo(double *, int, int) ;
/* Fichiers en entree */
char input_image_A[30];
char input_image_B[30];
char maple_Histo[30];

/* Images et taille de celles-ci */
DATA  *Image_A;
DATA  *Image_B;
int Xsize_A,Ysize_A;
int Xsize_B,Ysize_B;

/* Histogramme et nombre de cases */
double  *Histo;
int Taille_Histo;

/* intersection */
int p0_tmp, p1, S1, S2;
double p0, Inter, l;

/* Methode a executer */
int Exec_Meth;

/* ensemble de coupes de niveaux... */
int TmpCut[256];
int Cut[256];


/*************************************************/
/* Lecture du fichier de donnees                 */
/*************************************************/

void Lecture_Donnees(char *Param)
{
   FILE *lpFile;
 
   /* on ouvre le fichier de donnees en lecture */
   lpFile = fopen(Param,"rt") ;
 
   /* on lit le nom de la 1ere Image  */
   fscanf(lpFile,"%s",input_image_A);
 
   /* on lit la dimension de l'image */
   fscanf(lpFile,"%d  %d", &Xsize_A, &Ysize_A); 
 
   /* on lit le nom de la 2eme image  */
   fscanf(lpFile,"%s",input_image_B);
 
   /* on lit la dimension de l'image B */
   fscanf(lpFile,"%d  %d", &Xsize_B, &Ysize_B);

   /* Lecture de la taille de l'histo */
   fscanf(lpFile,"%d", &Taille_Histo);

   /* Lecture des differentes methodes a executer */
   fscanf(lpFile,"%d", &Exec_Meth);

   /* Histo resultat */
   fscanf(lpFile,"%s",maple_Histo);
   
   /* Valeur de p0 */
   fscanf(lpFile,"%d",&p0_tmp);
   p0=(double)p0_tmp/100;

   /* Valeur de p1 */
   fscanf(lpFile,"%d",&p1);
     
   fclose(lpFile);
}


/*************************************************/
/* Initilisation Globale                         */
/*************************************************/

void Initialisation()
{
  int i,j;
  // Image_A   = (DATA *)malloc(Xsize_A*Ysize_A*sizeof(DATA));
  // Image_B   = (DATA *)malloc(Xsize_A*Ysize_A*sizeof(DATA));

  Histo     = (double *)malloc(sizeof(double)*(Taille_Histo+1));    

  for(j=0;j<255;)
  {Cut[j]=0; TmpCut[j++]=0;}
  
  for(i=0;i<=Taille_Histo;Histo[i++]=0);
}

void Our_Methode(DATA *Image_A, DATA *Image_B, int Xsize, int Ysize, int Taille, double *Histo, int methode, int Cut[255], double l); 
/*************************************************/
/* Programme Principal                           */
/*************************************************/

double MyHistMethod(BinaryImage const& imgA, BinaryImage const& imgB)
{
  // FILE *lptemp, *f;
  double max, maxIndex;
  int k,t,j;
  struct TWL *Liste_Pt_Reg_A, *Liste_Pt_Reg_B;

  // fprintf(stderr,"%s\n",argv[1]);

  /* Lecture du fichier de donnees*/
  // Lecture_Donnees(argv[1]);

  Taille_Histo = 128;

  /* Initialisation */
  Initialisation();
  
  /* Lecture des differentes images */
  // readpicture(Image_A,Ysize_A,Xsize_A,input_image_A);
  // readpicture(Image_B,Ysize_B,Xsize_B,input_image_B);

 // TOUT CE QUI EST AVANT ICI NE SERT A RIEN !!!
 // Renommer cette fonction et la faire retourner un double(pas main quoi...)
 // Cette fonction prend en argument 2 qgarimage (binaryimage ?) et renvoie l'angle max


 // BinaryImage imgA(PbmFile(argv[1]));
 // Image_A = imgA.pPixMap();
 // Bien grer les autres paramtres des fonctions

	Image_A = imgA.pPixMap();
	Image_B = imgB.pPixMap();
	Xsize_A = imgA.width();
	Ysize_A = imgA.height();
	Xsize_B = imgB.width();
	Ysize_B = imgB.height();

  /* Permet de localiser les seuils inutiles */
  /* A reecrire ; a force d'en rajouter :) et en plus c pas bo */
  /* Definition de d : max entre diametre inter & diametre min */
  Liste_Pt_Reg_A = NULL;
  Liste_Pt_Reg_B = NULL;
  Liste_Pt_Reg_A = ListePoint(Image_A, Liste_Pt_Reg_A, Xsize_A, Ysize_A, TmpCut, &S1);
  Liste_Pt_Reg_B = ListePoint(Image_B, Liste_Pt_Reg_B, Xsize_B, Ysize_B, TmpCut, &S2);
  p0=1;
  p1=3;
  Exec_Meth = 5;
  // printf(" Liste points region A = %d\n",S1); 
  // printf(" Liste points region B = %d\n",S2); 
  for (k=1;k<=255;k++)
    if (TmpCut[k])
      Cut[++Cut[0]]=k;
  if (S1>S2) S1=S2;
  l=2*sqrt((double)S1/PI)*p0;
  Inter=0;
  for (t=0; t<Xsize_A*Ysize_A; t++)
    if (*(Image_A+t)&&*(Image_B+t))
      Inter ++;
  Inter=p1*2*sqrt((double)Inter/PI);
  if (Inter>l) l=Inter;
  /* Histogramme de forces */
if (Exec_Meth){
     if (Exec_Meth==9)
       {
	 /*Poly(Histo, Taille_Histo);*/
	 Angle_Histo(Histo, Taille_Histo, Exec_Meth);
       }
	 else{
       Our_Methode(Image_A, Image_B, Xsize_A, Ysize_A, Taille_Histo, Histo, Exec_Meth, Cut, l);
	   }
}else {
     /* Histogramme d'angles */
	  Histo_Angles(Histo, Taille_Histo, Liste_Pt_Reg_A, Liste_Pt_Reg_B); }
   
  /*   Angle_Histo(Histo, Taille_Histo, Exec_Meth);
       Ecriture_Histo(output_Histo, Histo, Taille_Histo); */
  int nbNull = 0;     
  max = 0;
  maxIndex = 0;
  for (j=0;j<Taille_Histo;j++){
  	if(Histo[j] == 0) ++nbNull;
  	else if (Histo[j]>max){
          max=Histo[j];
		  maxIndex = j;
	  }
  }

  // Rsultat entre 0 et 2PI
  // return (2 * PI * maxIndex) / Taille_Histo;

  // Rsultat entre 1 et 2
  if(!nbNull) return 2;
  else return 1 + maxIndex / Taille_Histo;

  // On s'arrte ici, on rcupre le l'index (j) du max.

  /*if(!(f=fopen(maple_Histo,"wb"))) printf("erreur\n");
  fprintf(f,"l1:=[");
  for (j=0;j<Taille_Histo;j++)
    fprintf(f,"[%d,%f],",j,Histo[j]/max);
  fprintf(f,"[%d,%f]]:",j,Histo[j]/max);

  fclose(f);*/

}
 








