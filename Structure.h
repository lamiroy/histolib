#include <stdlib.h>
#include <math.h>

#define DATA unsigned char 
#define TRUE  1
#define FALSE 0

#define PI 3.141592653589 
#define e_ln 2.73

/* Point barycentre */
struct POINT
{
  double Absi,Ordo;
}; 		


/* Point Classique */
struct point
{
  int x,y;
};

struct Adresse
{
  DATA *adr;
};


/* */
struct TWL
{
  /* Coordonnees */
  int X,Y;
  int Valeur;
  struct TWL *suivant;
};


/* Points cardinaux */
struct Orientation
{
  double Left;
  double Right;
  double Above;
  double Below;
};

/* Pour Chainage des Segments */
struct Segment
{
  int x1; 
  int y1;
  int x2; 
  int y2;
  int Val;
  struct Segment *suivant;
};





