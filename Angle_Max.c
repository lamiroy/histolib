/****************************************************************************
		    RELATIONS SPATIALES DIRECTIONNELLES
		     (P.Matsakis/L.Wendling/J.Desachy)


 Fichier Principal : lecture des donnees et execution des methodes

=============================================================================

 Date   | avril 1996
 Auteur | L.Wendling

============================================================================*/

#include "Structure.h"
#include <stdio.h>

/* Calcul des diverses orientations */
extern double Calcul_Mu_Sin_Miyajima(double *, int);

/* Methode de Krishnapuram */
extern double Calcul_Krishnapuram(double *, int, double);

/* Calcul de l'orientation */
extern double degre_relation(double *, int);

char input_Histo[30];

double *Histo2;
double *Histo_Shift;
double a;

/* Constante du trapeze, Fixer a 0 */
/* le cas echeant enlever le commentaire ds lecture donnees */

//int Exec_Meth;

/*************************************************/
/* Lecture du fichier de donnees                 */
/*************************************************/

//void Lecture_Donnees(char *Param)
//{
//   FILE *lpFile;
// 
//   /* on ouvre le fichier de donnees en lecture */
//   lpFile = fopen(Param,"rt") ;
// 
//   /* on lit le nom de l'histo  */
//   fscanf(lpFile,"%s",input_Histo);
// 
//   /* Lecture de la methode a executer */
//   fscanf(lpFile,"%d", &Exec_Meth);
//
//   fclose(lpFile);
//}


/*************************************************/
/* Initilisation Globale                         */
/*************************************************/

//void Initialisation(int Taille)
//{
//  Histo_Shift = (double *)malloc(sizeof(double)*(Taille+1)); 
//}


/*************************************************/
/* Lecture Histogramme - sans Header -           */
/* Le prevoir dans les versions futures...       */
/*************************************************/

void read_histo(char* nom_fichier, int *Taille)
{
  FILE* ptr_fichier;
  double bidon;
  int i;
  /* recherche de la taille ... */
  ptr_fichier=fopen(nom_fichier,"r");
  i=0;
  while (!feof(ptr_fichier)) {fscanf(ptr_fichier,"%lf ",&bidon);i++;}
  *Taille=--i;
  fclose(ptr_fichier);
  /* Remplissage du tableau */
  Histo2       = (double *)malloc(sizeof(double)*(*Taille+1));
  ptr_fichier=fopen(nom_fichier,"r");
  i=0;
  while (!feof(ptr_fichier)) fscanf(ptr_fichier,"%lf ",&Histo2[i++]);
  fclose(ptr_fichier);
}


/*************************************************/
/* Decalage suivant l'angle (il y a plus econo-  */
/*************************************************/

void shift_histo(double *Histo, double *Histo_Shift, double angle, int Taille_Histo)
{
  int i, case_op;
  for (i=0,case_op=(int)(Taille_Histo*angle/360.); i<Taille_Histo; i++, case_op++)
    Histo_Shift[case_op%Taille_Histo]=Histo[i];
  Histo_Shift[Taille_Histo]=Histo_Shift[0];
}


/*************************************************/
/* Programme Principal                           */
/*************************************************/

//main(int argc, char *argv[])
//{
//  int Taille_H, i,j;
//  double angle, pas_angle, tmp, val_opt, angle_opt, tmp_Val;
//  a=0.0; /* Par defaut, on fixe un triangle */
//  Lecture_Donnees(argv[1]);
//  read_histo(input_Histo, &Taille_H);
//  Initialisation(Taille_H);
//  
//  pas_angle=360.0/((double)Taille_H);
//  val_opt=0.0;
//  Histo_Shift      = (double *)malloc(sizeof(double)*(Taille_H+1));
//  
//  /* on peut s'interesser a ]-pi/2, pi/2] le resultat est le meme... */
//  /* on peut s'arreter a i<=Taille_H/2 */
//  angle_opt = 0.0;
//  /*printf("Taille = %d\nTaille_H);*/
// 
//  for (i=0, angle=0.0; i<Taille_H; i++, angle+=pas_angle)
//    {
//      /*      shift_histo(Histo, Histo_Shift, angle, Taille_H);*/
//      for (j=0;j<=Taille_H;j++) Histo_Shift[j]=Histo[j];
//      if (Exec_Meth)
//	if (Exec_Meth==1) /* Angle */
//	  {
//	    tmp=Calcul_Mu_Sin_Miyajima(Histo_Shift, Taille_H);
//	    if (tmp>val_opt) {val_opt=tmp;angle_opt=angle;}
//	  }
//	else
//	  {
//	    tmp=degre_relation(Histo_Shift, Taille_H/4);
//	    if (tmp>val_opt) {val_opt=tmp;angle_opt=angle;}
//	  }
//      else
//	  {
//	    tmp=Calcul_Krishnapuram(Histo_Shift, Taille_H, a);
//	    if (tmp>val_opt) {val_opt=tmp;angle_opt=angle;}
//	  }
//      /*printf("%lf ",tmp);*/
//      tmp_Val=Histo[Taille_H-1];
//      for (j=Taille_H;j>0;j--)
//	Histo[j]=Histo[j-1];
//      Histo[0]=tmp_Val;
//      /*if (i==Taille_H-1)
//	for (j=0;j<=Taille_H;j++)
//	  printf("%lf ",Histo[j]);*/
//
//   }
//  printf("Taille = %d et Valeur = %lf pour angle = %lf\n",  Taille_H, val_opt, 360-angle_opt);
//}      


    





