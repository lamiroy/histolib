/********************************************************/
/* Ce fichier regroupe l'ensemble des fonctions appeles */
/* en externe. A decomposer le cas echeant              */
/********************************************************/


/* Lecture d'une image*/
extern int readpicture(struct DATA *, int, int, char *);

/* Chainage des points des regions */
struct TWL *ListePoint(struct DATA *, TwoWayList *, int, int, int [], int *);

/* Calcul de l'histo version Miyajima-Khrisnapuram */
void Histo_Angles(double *,int, struct TWL *, struct TWL *);

/* Application de notre methode */
void Histo_Forces(struct DATA *, struct DATA *, int, int, int, double *, int, int[], double);

/* Methode polygonale */
void Poly(double *, int);

/*  Ecriture de l'histogramme */
extern void Ecriture_Histo(char *, double *, int);









