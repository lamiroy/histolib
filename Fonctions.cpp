#include "Structure.h"
#include <stdio.h>


/****************************************************************/
/******* Calcul de la liste des points de la regions ************/
/****************************************************************/

struct TWL *ListePoint (DATA *Image, struct TWL *Liste_Pt, int Xsize, int Ysize, int Cut[256], int *nbpt)
{
  struct TWL *Elem, *Deb;
  int paspasser;
  int nb, i, j;
  (*nbpt)=nb=0;
  paspasser=1;
  /*Bary->Absi=0;
  Bary->Ordo=0;*/
  Deb=NULL;
  for (i=0; i<Xsize*Ysize; i+=Xsize)
    for (j=0; j<Xsize;j++)
      if (*(Image+i+j))
	{
	  Elem = (struct TWL *)malloc(sizeof(struct TWL));
	  Elem->X=j;
	  Elem->Y=Ysize-1-(int)i/Xsize; /* A cause du repere */
	  /*	  Bary->Absi+=(double)Elem->X; 
	  Bary->Ordo+=(double)Elem->Y;*/
	  nb+=(int)*(Image+i+j);
	  Elem->Valeur=*(Image+i+j);
	  Cut[(int)*(Image+i+j)]++;
	  (*nbpt)++;
	  if (paspasser) 
	    { Deb=Elem; Liste_Pt=Elem; Liste_Pt->suivant=NULL; paspasser=0; }
          else 
	    { Liste_Pt->suivant=Elem; Liste_Pt=Liste_Pt->suivant;
	      Liste_Pt->suivant=NULL;}
	}
   return(Deb);
}



